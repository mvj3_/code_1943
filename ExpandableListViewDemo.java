//这个例子主要是ExpandableListView的实现，可扩展的listview是的组可以独立的显示其子项，层级为二
//这个例子在每个子项后面添加了删除图标，可删除任一子项
//1. 首先新建xml布局文件res/layout/main_activity.xml，主要定义expandablelistview
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity" >
    <ExpandableListView
        android:id="@+id/expan_list"
        android:layout_width="match_parent"
        android:layout_height="fill_parent" >
    </ExpandableListView>
</RelativeLayout>
//2.  新建组布局文件res/layout/group_list.xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:padding="10dp" >
    <TextView
        android:id="@+id/group_name"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:paddingLeft="25dp"/>
</LinearLayout>
//3. 新建子项目布局res/layout/child_list.xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="fill_parent"
    android:layout_height="wrap_content"
    android:padding="10dp" > 
    <TextView
        android:id="@+id/child_name"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentLeft="true"
        android:layout_centerVertical="true"
        android:paddingLeft="25dp" />
    <ImageView
        android:id="@+id/delete"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentRight="true"
        android:src="@drawable/ic_launcher"
        android:contentDescription="@string/app_name"/>
</RelativeLayout>
//4. 为expandalbelistview定义适配器，代码都很简单，没写什么注释
public class MyExpandableListAdapter extends BaseExpandableListAdapter {
	
	private Activity mContext;
	private Map<String, List<String>> collections; //数据源
	private List<String> groupItems; // 组名称

	public MyExpandableListAdapter(Activity context, Map<String, List<String>> mCollections,
			List<String> mGroupItems) {
		this.mContext = context;
		this.collections = mCollections;
		this.groupItems = mGroupItems;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return collections.get(groupItems.get(groupPosition)).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		final String childName = (String) getChild(groupPosition, childPosition);
		LayoutInflater inflater = mContext.getLayoutInflater();
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.child_list, null);
		}
		TextView childItem = (TextView) convertView.findViewById(R.id.child_name);
		ImageView delete = (ImageView) convertView.findViewById(R.id.delete);
		delete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
				builder.setMessage("Do you want to remove?");
				builder.setCancelable(false);
				builder.setTitle("Warning");
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						List<String> child = collections.get(groupItems.get(groupPosition));
						child.remove(childPosition);
						notifyDataSetChanged();
					}
				});
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				AlertDialog alertDialog = builder.create();
				alertDialog.show();
			}
		});
		childItem.setText(childName);
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return collections.get(groupItems.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return groupItems.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return groupItems.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		final String groupName = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater inflater = mContext.getLayoutInflater();
			convertView = inflater.inflate(R.layout.group_list, null);
		}
		TextView groupItem = (TextView) convertView.findViewById(R.id.group_name);
		groupItem.setTypeface(null, Typeface.BOLD);
		groupItem.setText(groupName);
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
//5. 在MainActivity中使用ExpandableListView
public class MainActivity extends Activity {
	
	private ExpandableListView expanListView;
	private MyExpandableListAdapter adapter;
	private Map<String, List<String>> collections;
	private List<String> groupCollection;
	private List<String> childCollection;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initGroupCollection();
		initCollections();
		expanListView = (ExpandableListView) findViewById(R.id.expan_list);
		adapter = new MyExpandableListAdapter(MainActivity.this, collections, groupCollection);
		expanListView.setAdapter(adapter);
		expanListView.setOnChildClickListener(new OnChildClickListener() {
			
			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				final String selected = (String) adapter.getChild(groupPosition, childPosition);
				Toast.makeText(MainActivity.this, selected, Toast.LENGTH_SHORT).show();
				return true;
			}
		});
	}
	
	public void initGroupCollection() {
		groupCollection = new ArrayList<String>();
		groupCollection.add("湖南");
		groupCollection.add("广东");
		groupCollection.add("江西");
		groupCollection.add("四川");
	}
	
	public void initCollections() {
		String[] hunanCities = {"长沙", "岳阳", "株洲", "永州"};
		String[] guangdongCities = {"广州", "深圳", "珠海", "东莞"};
		String[] jiangxiCities = {"南昌", "九江", "宜春", "鹰潭"};
		String[] sichuangCities = {"成都", "乐山", "达州", "内江"};
		collections = new LinkedHashMap<String, List<String>>();
		for (String groupName : groupCollection) {
			if (groupName.equals("湖南")) {
				loadChildItems(hunanCities);
			} else if (groupName.equals("广东")) {
				loadChildItems(guangdongCities);
			} else if (groupName.equals("江西")) {
				loadChildItems(jiangxiCities);
			} else if (groupName.equals("四川")) {
				loadChildItems(sichuangCities);
			}
			collections.put(groupName, childCollection);
		}
	}
	
	public void loadChildItems(String[] cities) {
		childCollection = new ArrayList<String>();
		for (String city : cities) {
			childCollection.add(city);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
效果图：

